<?php

namespace App\Models;

use App\Models\UserMenu;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserAccessMenu extends Model
{
    use HasFactory;
    
    protected $fillable = ['role_id', 'menu_id'];

    public function menu()
    {
        return $this->belongsTo(UserMenu::class, 'menu_id', 'id');
    }
}
