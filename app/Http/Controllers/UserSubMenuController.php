<?php

namespace App\Http\Controllers;

use App\Models\UserMenu;
use App\Models\UserSubMenu;
use Illuminate\Http\Request;

class UserSubMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // orm relation 'with' menu model
        $submenu = UserSubMenu::with('menu')
        ->orderBy('menu_id')
        ->get();

        return view('submenu.index', compact('submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = UserMenu::all();
        $submenu = UserSubMenu::all();
        return view('submenu.create', compact('submenu', 'menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();

        $submenu = UserSubMenu::where('menu_id', $req['menu_id'])
        ->orderBy('order', 'desc')
        ->first();

        if(!empty($submenu)){
            $order = $submenu->order + 1;
        }else{
            $order = 1;
        }

        // assign
        $req['order'] = $order;

        UserSubMenu::create($req);

        return redirect(route('submenu.index'))->with('status', 'Submenu baru berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserSubMenu  $submenu
     * @return \Illuminate\Http\Response
     */
    public function show(UserSubMenu $submenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserSubMenu  $submenu
     * @return \Illuminate\Http\Response
     */
    public function edit(UserSubMenu $submenu)
    {
        $menu = UserMenu::all();
        return view('submenu.edit', compact('submenu', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserSubMenu  $submenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserSubMenu $submenu)
    {
        $req = $request->all();
        $submenu->update($req);

        return redirect(route('submenu.index'))->with('status', 'Menu berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserSubMenu  $submenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserSubMenu $submenu)
    {
        UserSubMenu::destroy($submenu->id);

        return redirect(route('submenu.index'))->with('status', 'Menu berhasil dihapus!');
    }
}
