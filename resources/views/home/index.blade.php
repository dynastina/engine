@extends('layouts.main')
@section('title', 'Home')
@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Content-->
        <div class="content fs-6 d-flex flex-column-fluid" id="kt_content">
            <!--begin::Container-->
            <div class="container">
                {{-- disini --}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow-sm">
                            <div class="card-header">
                                <div class="card-title fs-1 fw-boldest text-uppercase text-center">Dashboard</div>
                            </div>
                            <div class="card-body">
                                <h3>Description goes here</h3>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- /disini --}}
            </div>
            <!--end::Container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
